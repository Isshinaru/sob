﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptMuniTempo : MonoBehaviour
{
    public GameObject munitions;
   
    public Text tropDeMun;
    public int munition = 0;

    int feuillePush = 0;
    int feuilleStepNum = 0;
    int alreadyTrasnsform = 0;
    [SerializeField] Button feuille;
    [SerializeField] GameObject newFeuille, arrow, arrow2, arrow3;
    [SerializeField] Sprite[] feuilleStep;
    [SerializeField] GameObject[] muni;
    [SerializeField] ChargeScript chargeScript;

    AudioSource macheSound, paperCut;
    // Start is called before the first frame update
    void Start()
    {
       
        AudioSource[] audiosMun = GetComponents<AudioSource>();
        macheSound = audiosMun[0];
        paperCut = audiosMun[1];
        if (GameObject.Find("TutoManager"))
            munition = 1000;
        
        newFeuille.SetActive(false);
       
       // munition++;
        
        tropDeMun.GetComponent<Text>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    public void OnClickMuni()
    {
        if(feuillePush < 10)
        {
            if(arrow2.activeSelf)
            {
                arrow2.SetActive(false);
                arrow3.SetActive(true);
            }
            paperCut.Play();
            muni[feuillePush].SetActive(true);
            feuillePush++;
            feuilleStepNum++;
            feuille.GetComponent<Image>().sprite = feuilleStep[feuilleStepNum];
            if (feuilleStepNum == 15)
            {
                feuille.enabled = false;
                newFeuille.SetActive(true);
            }
        }
        else
        {
            //Ethan jsp du tout comment est codé le tir donc esseye juste de faire en sorte que lorsque tu tires ca decremente le nombre de muni.
            StartCoroutine("TextMaxMun");
        }
    }

    IEnumerator TextMaxMun()
    {
        tropDeMun.GetComponent<Text>().enabled = true;
        yield return new WaitForSeconds(2f);
        tropDeMun.GetComponent<Text>().enabled = false;
        
    }
    public void OnClickNewFeuille()
    {
        feuilleStepNum = 0;
        feuille.enabled = true;
        feuille.GetComponent<Image>().sprite = feuilleStep[0];
        newFeuille.SetActive(false);
    }
    public void OnClickTemp()
    {
        
        if (arrow.activeSelf)
        {
            arrow.SetActive(false);
            arrow3.SetActive(false);
        }
        if (munition == 0 && feuillePush > 0)
            chargeScript.StartCoroutine(chargeScript.ReloadProj());
        for(int i=alreadyTrasnsform; i<feuillePush;i++)
        {
            macheSound.Play();
            muni[i].GetComponent<Animator>().SetBool("boule", true);
            munition++;
            alreadyTrasnsform++;
        }
        
    }
    public void MunMoins()
    {
        if (munition <= 0)
        {
            arrow.SetActive(true);
            arrow2.SetActive(true);

           
            return;
        }
        if (munition <= 1)
        {

            
            arrow.SetActive(true);
            arrow2.SetActive(true);
        }
        alreadyTrasnsform--;
        munition--;
        feuillePush--;
        if (feuillePush != munition)
        {
            muni[munition].GetComponent<Animator>().SetBool("boule", false);
        }
        muni[feuillePush].GetComponent<Animator>().SetBool("boule", false);
        muni[feuillePush].SetActive(false);
    }
    public void MunForTuto()
    {
        munition = 1;
    }
}
