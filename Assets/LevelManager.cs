﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    [SerializeField] List<GameObject> ballon;
    [SerializeField] List<PlaneFly> avion;
    [SerializeField] GameObject QuestionMan, diplome, diplomeText, door, auCoin, virer;
    [SerializeField] Camera cameraDoor, cameraCoin;
    [SerializeField] GameObject canvasJ1, canvasJeu;
    [SerializeField] ScoreManager scoreManager;
    int numberballonTotal = 12, numberAvionTotal = 13;
    int nombreBallonActifs, nombreAvionsActifs;
    public bool isMobile, isTuto;
    AudioSource ring;
   
    // Start is called before the first frame update
    void Start()
    {
       
        AudioSource[] endAudio = GetComponents<AudioSource>();
        ring = endAudio[0];

        if (SceneManager.GetActiveScene().name == "Game")
        {
          
         
            isTuto = false;
            for (int i = 0; i <= 3; i++)
            {
                ballon[i].SetActive(true);
                nombreBallonActifs++;
                avion[i].enabled = true;
                nombreAvionsActifs++;
            }
            StartCoroutine(BallonSpawn());
            StartCoroutine(AvionSpawn());
        }
        else if(SceneManager.GetActiveScene().name == "Tutos")
        {
            isTuto = true;
        }
    }

    // Update is called once per frame
    IEnumerator BallonSpawn()
    {
        while (nombreBallonActifs < numberballonTotal)
        {

            yield return new WaitForSeconds(20f);
            ballon[nombreBallonActifs - 1].SetActive(true);
            nombreBallonActifs++;
        }
    }
    IEnumerator AvionSpawn()
    {
        while (nombreAvionsActifs < numberAvionTotal)
        {

            yield return new WaitForSeconds(18f);
            avion[nombreAvionsActifs - 1].enabled = true;
            nombreAvionsActifs++;
        }
    }
    void DesactivateMovingObjectWhenQuestions()    //A modifier si on enleve les objets pendant les questions pour que les ballons s'implementent quand meme 
    {
        foreach (GameObject ball in ballon)
        {
            ball.SetActive(false);
        }
    }
    public void TimeEndOrScoreLimitEnd()
    {
        ring.Play();
        canvasJ1.SetActive(false);
        
        diplome.SetActive(true);
        StartCoroutine(DiplomeText());
    }
    public void GameOver()
    {
        StartCoroutine(EndGame());
    }
    public void PuniCoin()
    {
        StartCoroutine(Punition());
    }
    IEnumerator DiplomeText()
    {
       
        yield return new WaitForSeconds(0.2f);
        diplomeText.SetActive(true);
        int score = scoreManager.player1Score;
        if (PlayerPrefs.GetInt("Score") < score)
            PlayerPrefs.SetInt("Score", (int)score);


    }
   
    IEnumerator EndGame()
    {
        canvasJ1.SetActive(false);
        canvasJeu.SetActive(false);
        virer.SetActive(true);
        cameraDoor.depth = 3;
        int a = 0;
        while (a < 360)
        {
            if (a > 60 && virer.activeSelf)
                virer.SetActive(false);
            door.transform.Rotate(0, -0.25f, 0);
            a++ ;
            yield return new WaitForEndOfFrame();
        }

        
        if (isTuto)
            SceneManager.LoadScene("Tutos");
        else
            SceneManager.LoadScene("Menu");
        
    }

    IEnumerator Punition()
    {
        canvasJ1.SetActive(false);
        auCoin.SetActive(true);
        yield return new WaitForSeconds(1f);
        cameraCoin.depth = 3;
        auCoin.SetActive(false);
        yield return new WaitForSeconds(5f);
        canvasJ1.SetActive(true);
        cameraCoin.depth = 0;
    }
    public void OnClick(int num)
    {
        switch (num)
        {
            case 0:
                SceneManager.LoadScene("Game");
                break;
            case 1:
                SceneManager.LoadScene("Menu");
                break;
            default:
                break;
        }
    }
}
