﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderinLayer : MonoBehaviour
{
    public int sort;
    // Use this for initialization
    void Start()
    {
        gameObject.GetComponent<Renderer>().sortingOrder = sort;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
