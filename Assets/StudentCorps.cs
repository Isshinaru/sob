﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StudentCorps : MonoBehaviour
{
    [SerializeField] ScoreManager scoreManager;
    bool interact = true;
    Animator anim;   //3 anims
    [SerializeField] Collider teteColl, couColl;
    [SerializeField] TextMeshPro pointText;
    AudioSource tete, bras;
    //   AudioSource collideSound;
    void Start()
    {
        pointText.text = "";
        AudioSource[] charaSound = GetComponents<AudioSource>();
        tete = charaSound[0];
        bras = charaSound[1];
        anim = GetComponent<Animator>();
       // collideSound = GetComponent<AudioSource>();
    }
    

    // Update is called once per frame
    void Update()
    {
        if (scoreManager.numberOfDownStudents >= scoreManager.maxDown)
            StartCoroutine(ReturnToIdle());
            
    }
    public void GoToScore(int colliderNumber, int playerNum, int scoreTouchPart = 1, int rank = 1, int isMovingObject = 1, int bonusPoint = 1)
    {
        if (interact)
        {
            switch (colliderNumber)
            {
                case 0:
                    //collideSound.Play();
                    break;
                case 1:
                    StartCoroutine(TurnRight());
                    break;
                case 2:
                    StartCoroutine(TurnLeft());
                    break;
                case 3:
                    interact = false;
                    teteColl.enabled = false;        //Collider qui se baisse pas super utile donc voila
                    couColl.enabled = false;
                    anim.SetBool("Hide", true);
                    scoreManager.numberOfDownStudents++;
                    tete.Play();
                    break;
                default:
                    break;

            }
            
            
            pointText.text = "+ " + scoreTouchPart * rank * isMovingObject * bonusPoint;
            if (GameObject.Find("Main CameraJ1").GetComponent<Camera>().enabled)
                pointText.transform.LookAt(2 * pointText.transform.position - Camera.main.transform.position);
            scoreManager.Score(playerNum, scoreTouchPart, rank, isMovingObject, bonusPoint);
            StartCoroutine(DisableScoreText());

        }
    }
    IEnumerator DisableScoreText()
    {
        yield return new WaitForSeconds(1f);
        pointText.text = "";
    }
    IEnumerator TurnRight()
    {
        bras.Play();
        interact = false;
        anim.SetTrigger("Right");
        yield return new WaitForSeconds(1f);
        interact = true;
    }
    IEnumerator TurnLeft()
    {
        bras.Play();
        interact = false;
        anim.SetTrigger("Left");
        yield return new WaitForSeconds(1f);
        interact = true;
    }
    IEnumerator ReturnToIdle()
    {
        anim.SetBool("Hide", false);
        yield return new WaitForSeconds(1f);
        teteColl.enabled = true;       
        couColl.enabled = true;
        interact = true;
    }
   
}
