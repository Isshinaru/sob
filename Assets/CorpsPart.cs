﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorpsPart : MonoBehaviour
{
    private StudentCorps studentCorps;
    [SerializeField] int partNumber, pointPart, rank;
    void Start()
    {
        studentCorps = gameObject.GetComponentInParent<StudentCorps>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter(Collision other)
    {
        switch(other.gameObject.layer)
        {
            case 10:
                
                studentCorps.GoToScore(partNumber, 1, pointPart, rank, 1, 1);
                break;
            case 11:
                studentCorps.GoToScore(partNumber,2, pointPart, rank, 1, 1);
                break;
            case 12:
                studentCorps.GoToScore(partNumber, 3, pointPart, rank, 1, 1);
                break;
            case 13:
                studentCorps.GoToScore(partNumber, 4, pointPart, rank, 1, 1);
                break;
            default:
                break;
        }
    }
}
