﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TutoManager : MonoBehaviour
{
    [SerializeField] GameObject shootStepSlid, scriptContainerShoot, shootStepMouse, munStep, munButton, questionStep, textBox, returnTuto, weapon;
    [SerializeField] List<GameObject> button;
    [SerializeField] Text tutoText;
    [SerializeField] LevelManager levelManager;
    [SerializeField] CameraController cameraController;
   
    int steps = 0;
    int tutoNum;
    void Awake()
    {
        cameraController.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E) && tutoNum==1)
        {
            if (steps == 2)
            {
                cameraController.SwitchCam(0);
                OnClickTutoShoot();
            }
        }
    }
    public void OnClickTutoStep(int step)
    {
        switch(step)
        {
            case 0:
                tutoNum = 0;
                foreach (GameObject but in button)
                    but.SetActive(false);
                OnClickTutoShoot();
                break;
            case 1:
                tutoNum = 1;
                foreach (GameObject but in button)
                    but.SetActive(false);
                OnClickTutoShoot();
                break;
            case 2:
                tutoNum = 2;
                foreach (GameObject but in button)
                    but.SetActive(false);
                OnClickTutoShoot();
                break;
            case 3:
                SceneManager.LoadScene("Tutos");           
                break;
            case 4:
                SceneManager.LoadScene("Menu");
                break;


        }
    }
    public void OnClickTutoShoot()
    {
        switch (tutoNum)
        {
            case 0:
                switch (steps)
                {
                    case 0:
                        textBox.SetActive(true);
                        tutoText.text = "Vous allez pouvoir tirer sur les élèves pour vous familiariser avec le système de tir et vous améliorer pour marquer un maximum de points.";
                        break;
                    case 1:
                        tutoText.text = "En temps normal vous aurez besoin de fabriquer des munitions mais on vous laisse ça dans un autre tuto. \nGo!!!";
                        break;
                    case 2:
                        cameraController.enabled = true;
                        textBox.SetActive(false);
                        scriptContainerShoot.SetActive(true);
                        if (levelManager.isMobile==true)shootStepSlid.SetActive(true);
                        else shootStepMouse.SetActive(true);
                        returnTuto.SetActive(true);
                       
                        
                        break;

                }
                break;
            case 1:
                switch (steps)
                {
                    case 0:
                        textBox.SetActive(true);
                        tutoText.text = "Ah mince ! Vous n'avez plus de munitions. Laissez-moi vous guider. ";
                        break;
                    case 1:
                        textBox.SetActive(false);
                        munStep.SetActive(true);
                        break;
                    case 2:
                        textBox.SetActive(true);
                        if(levelManager.isMobile)
                            tutoText.text = "Re ! Laissez-moi lister les étapes de la création : \n1. Cliquez sur la feuille\n 2.Faites du bruit dans votre micro\nVoilà ! Vous savez tout, reste plus qu'à essayer !";
                        else
                            tutoText.text = "Re ! Laissez-moi lister les étapes de la création : \n1. Cliquez sur la feuille\n 2.Appuyez sur la bouche\nVoilà ! Vous savez tout, reste plus qu'à essayer !";
                        break;
                    case 3:
                        textBox.SetActive(false);
                        munButton.SetActive(true);
                        returnTuto.SetActive(true);
                        break;

                }
                break;
            case 2:
                switch (steps)
                {
                    case 0:
                        weapon.SetActive(false);
                        textBox.SetActive(true);
                        tutoText.text = "Hé ! \nVous n'êtes pas là pour vous reposer ! \nLe prof vous a toujours à l'oeil ! ";
                        break;
                    case 1:
                        tutoText.text = "Le prof va chercher à vous poser des questions pour voir si vous suivez le cours. Alors même si tirer c'est fun, regardez le tableau si vous ne voulez pas être punis !";
                        break;
                    case 2:
                        cameraController.enabled = true;
                        textBox.SetActive(false);
                        questionStep.SetActive(true);
                        returnTuto.SetActive(true);
                        break;

                }
                break;
            default:
                break;
        }

                steps++;
    }
   
}
