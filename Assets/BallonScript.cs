﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BallonScript : MonoBehaviour
{
   
    int rank;     //multiplicateur de score selon le rang de spawn
    public GameObject ballon;
    public ParticleSystem ballonExplode;
    float startPos;
    [SerializeField] ScoreManager scoreManager;
    [SerializeField] TextMeshPro scoreText;
    Collider ballColl;
    AudioSource explode;
    void Start()
    {
        scoreText.text = "";
        ballColl = gameObject.GetComponent<CapsuleCollider>();
        startPos = gameObject.transform.position.y;
        explode = GetComponent<AudioSource>();
        TpPosition();
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = new Vector3(transform.position.x +Random.Range(-0.001f,0.001f), transform.position.y + Time.deltaTime , transform.position.z );
     
      
        if (gameObject.transform.position.y >= -279f)
        {
            TpPosition();
           
        }
    }
    void TpPosition()
    {
        ballColl.enabled = true;
        gameObject.transform.position = new Vector3(transform.position.x,startPos, transform.position.z);
        ballon.SetActive(true);
        float posForRank = Random.Range(137.5f, 223f);
        transform.position = new Vector3(Random.Range(279f, 369f), transform.position.y, posForRank);
        if (posForRank < 160f)
            rank = 1;
        else if (posForRank >= 160 && posForRank < 182f)
            rank = 2;
        else if (posForRank >= 182f && posForRank < 205f)
            rank = 3;
        else if (posForRank >= 205 && posForRank < 223f)
            rank = 4;
        else
            rank = 5;

    }
    void OnCollisionEnter(Collision other)
    {      
        if (other.gameObject.layer == 10)
        {
                scoreManager.Score(1, 5, rank, 2, 1);
                scoreText.text = "+ " + 5 * rank * 2 * 1;
                scoreText.transform.LookAt(2 * scoreText.transform.position - Camera.main.transform.position);
                StartCoroutine(TpWhenExplode()); 
        }
    }
    IEnumerator TpWhenExplode()
    {
        explode.Play();
        ballColl.enabled = false;
        ballon.SetActive(false);
        ballonExplode.Play();
        yield return new WaitForSeconds(2f);
        scoreText.text = "";
        TpPosition();
    }
}


