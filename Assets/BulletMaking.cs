﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;

public class BulletMaking : MonoBehaviour
{
    private int minFreq;
    private int maxFreq;

    [SerializeField] Text logs;
    
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource.GetComponent<AudioSource>();
        Microphone.GetDeviceCaps(null, out int minFreq, out int maxFreq);

        //Pour savoir si les autorisations mic sont ok
        if (Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            logs.text = "has authorized";

        }
        else
        {
            logs.text = "has not authorized";
            Permission.RequestUserPermission(Permission.Microphone);
        }
    }

    IEnumerator LogsAff()
    {
        yield return new WaitForSeconds(1);
    }
}
