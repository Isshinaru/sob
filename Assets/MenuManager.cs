﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MenuManager : MonoBehaviour
{

    //All Buttons Functions
    [SerializeField] GameObject extraPannel;
    [SerializeField] Text maxScore;

    //Lancer une partie
    void Start()
    {

        if (PlayerPrefs.HasKey("Score"))
            maxScore.text = "Meilleur Score : " + PlayerPrefs.GetInt("Score") + " points";
        else
            maxScore.text = "Meilleur Score : Vide";
    }
    public void OnCLickPlay()
    {
        SceneManager.LoadScene("Game"); 
    }

    //Quitter
    public void OnClickLeave()
    {
        Application.Quit();
    }

    //TutoScene
    public void OnClickTuto()
    {
        SceneManager.LoadScene("Tutos");

    }
    public void OnClickExtra(int step)
    {
        switch(step)
        {
            case 0:
                extraPannel.SetActive(true);                
                break;
            case 1:                          
                extraPannel.SetActive(false);
                break;
            default:
                break;
               
        }
    }
}
