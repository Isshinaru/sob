﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class ScoreManager : MonoBehaviour
{
    public int player1Score, player2Score, player3Score, player4Score;
    public Text[] scoreTextPlayer1;
    public Text[] scoreTextPlayer2;
    public Text[] scoreTextPlayer3;
    public Text[] scoreTextPlayer4;
    
    public int numberOfDownStudents;
    public int maxDown;
    [SerializeField] Text scoreFinalText;
    void Start()
    {
        foreach(Text playerText in scoreTextPlayer1)
        {
            playerText.text = "J1 : " + player1Score + " pts";
        }
        foreach (Text playerText in scoreTextPlayer2)
        {
            playerText.text = "J2 : " + player2Score + " pts";
        }
        foreach (Text playerText in scoreTextPlayer3)
        {
            playerText.text = "J3 : " + player3Score + " pts";
        }
        foreach (Text playerText in scoreTextPlayer4)
        {
            playerText.text = "J4: " + player4Score + " pts";
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Score(int playerNum, int scoreTouchPart=1, int rank =1, int isMovingObject =1, int bonusPoint =1)
    {

      switch(playerNum)
        {
            case 1:
                player1Score += scoreTouchPart * rank * isMovingObject * bonusPoint;
                foreach (Text playerText in scoreTextPlayer1)
                {
                    playerText.text = "J1 : " + player1Score + " pts";
                    if (player1Score > player4Score && player1Score > player3Score && player1Score > player2Score)
                        scoreFinalText.text = "" + player1Score + " points";
                }
                break;
            case 2:
                player2Score += scoreTouchPart * rank * isMovingObject * bonusPoint;
                foreach (Text playerText in scoreTextPlayer2)
                {
                    playerText.text = "J2: " + player2Score + " pts";
                    if (player2Score > player1Score && player2Score > player3Score && player2Score > player4Score)
                        scoreFinalText.text = "" + player2Score + " points";
                }
                break;
            case 3:
                player3Score += scoreTouchPart * rank * isMovingObject * bonusPoint;
                foreach (Text playerText in scoreTextPlayer3)
                {
                    playerText.text = "J3 : " + player3Score + " pts";
                    if (player3Score > player1Score && player3Score > player4Score && player3Score > player2Score)
                        scoreFinalText.text = "" + player3Score + " points";
                }
                break;
            case 4:
                player4Score += scoreTouchPart * rank * isMovingObject * bonusPoint;
                foreach (Text playerText in scoreTextPlayer4)
                {
                    playerText.text = "J4 : " + player4Score + " pts";
                    if (player4Score > player1Score && player4Score > player3Score && player4Score > player2Score)
                        scoreFinalText.text = "" + player4Score + " points";

                }
                break;
            default:
                break;
        }
        if (numberOfDownStudents >= maxDown-1)
            StartCoroutine(StudentsUndown());

    }
    IEnumerator StudentsUndown()
    {
        yield return new WaitForSeconds(2f);
        numberOfDownStudents++;
        yield return new WaitForSeconds(0.5f);
        numberOfDownStudents = 0;
    }
   
}
