﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class IaStudent
{
    int points = 10;
    public bool isDown;
}
public class Student : MonoBehaviour
{
    Animator downStudent;
    public int damage = 10; //bullet damage
    public int life = 100; //ia student life

    public void Start()
    {
        downStudent = gameObject.GetComponent<Animator>();
    }


    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == 10)
        {
            Debug.Log("collision");
            life = life -= damage;

            if(life <= 0)
            {
                downStudent.SetBool("down", true);
                StartCoroutine(returnToIdle());
            }
        }
    }

    IEnumerator returnToIdle()
    {
        yield return new WaitForSeconds(10f);
        downStudent.SetBool("down", false);
    }
  
}
