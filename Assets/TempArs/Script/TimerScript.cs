﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Threading;

public class TimerScript : MonoBehaviour
{
    [SerializeField] GameObject questionManagerObj, questionUImages, questionUI;
    [SerializeField] LevelManager levelManager;
    TextMeshProUGUI chrono;
    [SerializeField] TextMeshPro tableau;
    [SerializeField] QuestionManager questionManager;
    int secondsLeft;
    AudioSource backgroundSound;
    public bool isEnd = false;
    void Start()
    {
        backgroundSound = GetComponent<AudioSource>();
        chrono = this.GetComponentInChildren<TextMeshProUGUI>();
        secondsLeft = 300 ;
        StartCoroutine(Timer());
    }

    // Update is called once per frame
    IEnumerator Timer()
    {
        int minutes = 5;
        int secondes = 0;
        string secToStrings="00";
        while (secondsLeft > 60)
        {
            minutes = Mathf.FloorToInt(secondsLeft / 60f);
            secondes = secondsLeft - minutes * 60;
            if (secondes < 10)
            {
                secToStrings = "0" + secondes;
            }
            else
            {
                secToStrings =""+ secondes;
            }
            chrono.text = minutes + ":" + secToStrings;
            yield return new WaitForSecondsRealtime(1f);
            secondsLeft--;
        }
        chrono.color = Color.red;
        isEnd = true;
        bool isQuestion = true;
       /* if (questionUI.activeSelf)
            isQuestion = true;
        else
        {
            questionManagerObj.SetActive(false);
            questionUImages.SetActive(false);
            isQuestion = false;
            tableau.text = "Etudes personnelles";
        }*/
        if(questionManager.questionTotal>=7)
        {
            isQuestion = false;
            questionManagerObj.SetActive(false);
            questionUImages.SetActive(false);
            tableau.text = "Etudes personnelles";
        }
       
        backgroundSound.pitch = 2;
        while (secondsLeft > 0)
        {
            if (questionManager.questionTotal >= 7 && isQuestion)
            {
                questionManagerObj.SetActive(false);
                questionUImages.SetActive(false);
                tableau.text = "Etudes personnelles";
                isQuestion = false;
            }
            minutes = Mathf.FloorToInt(secondsLeft / 60f);
            secondes = secondsLeft - minutes * 60;
            if (secondes < 10)
            {
                secToStrings = "0" + secondes;
            }
            else
            {
                secToStrings = "" + secondes;
            }
            chrono.text = minutes + ":" + secToStrings;
            yield return new WaitForSecondsRealtime(1f);
            secondsLeft--;
        }
        //EndGame
        levelManager.TimeEndOrScoreLimitEnd();
    }
}
