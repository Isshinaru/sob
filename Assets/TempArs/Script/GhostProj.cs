﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostProj : MonoBehaviour 
{ 
    // Start is called before the first frame update
    void Start()
    {

    }
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Plane")
        {
            this.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            this.gameObject.transform.position = new Vector3(0, -50, 0);
        }
    }
}
