﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuestionManager : MonoBehaviour
{
    [SerializeField] CameraController camContr;
    [SerializeField] GameObject QuestionUI, BoardImages, slider, menuTableButton, UiPc;
    [SerializeField] Image fillbarQuestion;
    [SerializeField] TextMeshPro Board;
    [SerializeField] SpriteRenderer[] imageNum;
    [SerializeField] Sprite[] images;
    Dictionary<string, Sprite> dict; 
    [SerializeField] TextMeshProUGUI Question, Answer1, Answer2, Answer3;
    [SerializeField] Button ans1,ans2,ans3;
    [SerializeField] GameObject[] blames;
    [SerializeField] LevelManager levelManager;
    [SerializeField] Animator senseiAnim;
    [SerializeField] Camera cameraJeu;
    [SerializeField] LayerMask normalCMask;
    [SerializeField] LayerMask questionMask;
    string[,] questions;
    List<int> questionPool;
    int currentAnswer,currentBlame;
    Color baseColor;
    AudioSource wrong, right;
    public int questionTotal = 0;
    // Start is called before the first frame update
    void Start()
    {
        AudioSource[] questionState = GetComponents<AudioSource>();
        right = questionState[0];
        wrong = questionState[1];

        baseColor = ans1.image.color;
        currentAnswer = 0;
        currentBlame = 0;
        dict = new Dictionary<string, Sprite>();
        SetUpDict();
        questions=new string[5, 13];//0:type de la Q ; 1: R1 ; 2:R2 ; 3: R3; 4: Question
        questionPool = new List<int>();// On stock les index des question dans une pool que l'on videra au furet à mesure.
        for(int i = 0; i < 12; i++)
        {
            questionPool.Add(i);
        }
        SetQuestionArray();
        RandomizeQuestion();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void RandomizeQuestion()
    {
        
        int nextQIndex = Random.Range(0, questionPool.Count);
        if (questionPool.Count == 0) return;
        int nextQ = questionPool[nextQIndex];
        switch (questions[0, nextQ]){
            case "Type1":
                StartCoroutine(QuestionType1(nextQ));
                break;
            case "Type2":
                StartCoroutine(QuestionType2(nextQ));
                break;
            case "Type3":
                StartCoroutine(QuestionType3(nextQ));
                break;
            case "Type4":
                StartCoroutine(QuestionType4(nextQ));
                break;
            case "Type5":
                StartCoroutine(QuestionType5(nextQ));
                break;
            default:
                break;
        }
        questionPool.RemoveAt(nextQIndex);
    }
    public void PlayerAnswer(int i)
    {
        if (i == currentAnswer)
        {
            switch(i)
            {
                case 1:
                    ans1.image.color = Color.green;
                    break;
                case 2:
                    ans2.image.color = Color.green;
                    break;
                case 3:
                    ans3.image.color = Color.green;
                    break;
                default:
                    break;
            }
            StopAllCoroutines();
            StartCoroutine(GoodAnswer());
        }
        else
        {
            switch (i)
            {
                case 1:
                    ans1.image.color = Color.red;
                    break;
                case 2:
                    ans2.image.color = Color.red;
                    break;
                case 3:
                    ans3.image.color = Color.red;
                    break;
                default:
                    break;
            }
            StopAllCoroutines();
            StartCoroutine(BadAnswer(i));
        }
    }
    IEnumerator QuestionType1(int nextQ)// type numero text
    {
        // tentative de mélange des réponse mais c'est codé avec le fion
        string actualQ="";
        int answer=Random.Range(0,3);
        int[] order = new int[3];
        order[0] = Random.Range(1, 4);
        order[1] = Random.Range(1, 4);
        while (order[1] == order[0])
        {
            order[1] = Random.Range(1, 4);
        }
        order[2] = 1;
        while (order[2] == order[0] || order[2] == order[1])
        {
            order[2]++;
        }
        currentAnswer = order[answer];
        // la réponse devrait être : question[order[answer],nextQ]
        //On affiche les trois string à la suite, faudra trouver un effet sympa maispour l'instant c'est utilitaire
        Board.text = questions[order[0], nextQ];
        yield return new WaitForSecondsRealtime(10f);
        Board.text = questions[order[1], nextQ];
        yield return new WaitForSecondsRealtime(10f);
        Board.text = questions[order[2], nextQ];
        yield return new WaitForSecondsRealtime(10f);
        Board.text = "";
        senseiAnim.SetBool("Question", true);
        yield return new WaitForSecondsRealtime(2f);
        // choix de la proie du prof
        switch (answer)
        {
            case 0:
                actualQ = questions[4, nextQ] + " ai-je écrit au tableau en 1er?";
                break;
            case 1:
                actualQ = questions[4, nextQ] + " ai-je écrit au tableau en 2ème?";
                break;
            case 2:
                actualQ = questions[4, nextQ] + " ai-je écrit au tableau en 3ème?";
                break;
        }
        Question.text = actualQ;
        Answer1.text = questions[1, nextQ];
        Answer2.text = questions[2, nextQ];
        Answer3.text = questions[3, nextQ];
        if (levelManager.isMobile == true) slider.SetActive(false);
        else UiPc.SetActive(false);
        menuTableButton.SetActive(false);
        camContr.SwitchCam(1);
        QuestionUI.SetActive(true);
        ans1.interactable = true;
        ans2.interactable = true;
        ans3.interactable = true;

        cameraJeu.cullingMask = questionMask;         //Pour ne pas voir l'arme et la boulette

        float timeLeft = 4f;
        while (timeLeft > 0f)
        {
            timeLeft -= Time.deltaTime;
            fillbarQuestion.fillAmount = 0.2f * timeLeft;
            yield return new WaitForSecondsRealtime(Time.deltaTime);
        }
       
        StartCoroutine(BadAnswer(0));
        // temps écoulé, sanction
    }
    IEnumerator QuestionType2(int nextQ)// type image (nombre)
    {
        
        int answer = Random.Range(1, 4);
        Question.text = "Combien "+questions[answer,nextQ]+" y avait-il au tableau?";
        int numAns = 0;
        for(int i = 0; i < 10; i++)
        {
            int tmp = Random.Range(1, 4);
            if (tmp == answer) numAns++;
            imageNum[i].sprite = dict[questions[tmp,nextQ]];
        }
        if (numAns == 0)
        {
            Answer1.text = ""+0;
            Answer2.text = ""+1;
            Answer3.text = ""+2;
            currentAnswer = 1;
        }else if (numAns == 1)
        {
            switch (Random.Range(0, 2))
            {
                case 0:
                    Answer1.text = "" + 0;
                    Answer2.text = "" + 1;
                    Answer3.text = "" + 2;
                    currentAnswer = 2;
                    break;
                case 1:
                    Answer1.text = "" + 1;
                    Answer2.text = "" + 2;
                    Answer3.text = "" + 3;
                    currentAnswer = 1;
                    break;
            }
        }else if (numAns == 9)
        {
            switch(Random.Range(0, 2))
            {
                case 0:
                    Answer1.text = "" + 7;
                    Answer2.text = "" + 8;
                    Answer3.text = "" + 9;
                    currentAnswer = 3;
                    break;
                case 1:
                    Answer1.text = "" + 8;
                    Answer2.text = "" + 9;
                    Answer3.text = "" + 10;
                    currentAnswer = 2;
                    break;
            }
        }else if (numAns == 10)
        {
            Answer1.text = "" + 8;
            Answer2.text = "" + 9;
            Answer3.text = "" + 10;
            currentAnswer = 3;
        }
        else
        {
            switch (Random.Range(0, 3))
            {
                case 0:
                    Answer1.text = "" + (numAns-2);
                    Answer2.text = "" + (numAns-1);
                    Answer3.text = "" + numAns;
                    currentAnswer = 3;
                    break;
                case 1:
                    Answer1.text = "" + (numAns-1);
                    Answer2.text = "" + numAns;
                    Answer3.text = "" + (numAns+1);
                    currentAnswer = 2;
                    break;
                case 2:
                    Answer1.text = "" + numAns;
                    Answer2.text = "" + (numAns+1);
                    Answer3.text = "" + (numAns+2);
                    currentAnswer = 1;
                    break;
            }
        }
        BoardImages.SetActive(true);
        yield return new WaitForSecondsRealtime(18f);
        BoardImages.SetActive(false);
        senseiAnim.SetBool("Question", true);
        yield return new WaitForSecondsRealtime(2f);
        if (levelManager.isMobile == true) slider.SetActive(false);
        else UiPc.SetActive(false);

        menuTableButton.SetActive(false);
        camContr.SwitchCam(1);
        QuestionUI.SetActive(true);
        ans1.interactable = true;
        ans2.interactable = true;
        ans3.interactable = true;

        cameraJeu.cullingMask = questionMask;   //Pour ne pas voir l'arme et la boulette

        float timeLeft = 4f;
        while (timeLeft > 0f)
        {
            timeLeft -= Time.deltaTime;
            fillbarQuestion.fillAmount = 0.2f * timeLeft;
            yield return new WaitForSecondsRealtime(Time.deltaTime);
        }
        //yield return new WaitForSecondsRealtime(5f);
        
        StartCoroutine(BadAnswer(0));
    }
    IEnumerator QuestionType3(int nextQ)// type culture G
    {
        yield return null;
    }
    IEnumerator QuestionType4(int nextQ)// type shiritori
    {
        yield return null;
    }
    IEnumerator QuestionType5(int nextQ)
    {
        yield return null;
    }
    IEnumerator GoodAnswer()
    {
        

        right.Play();
        senseiAnim.SetBool("Question", false);
        ans1.interactable = false;
        ans2.interactable = false;
        ans3.interactable = false;
        fillbarQuestion.fillAmount = 0f;
        Question.text = "Bien.";
        yield return new WaitForSecondsRealtime(2f);
        QuestionUI.SetActive(false);
        if (levelManager.isTuto == false)
        {   
            if (levelManager.isMobile == true) slider.SetActive(true);
            else UiPc.SetActive(true);
            menuTableButton.SetActive(true);
        }
        yield return new WaitForSecondsRealtime(2f);
        cameraJeu.cullingMask = normalCMask;
        ans1.image.color = baseColor;
        ans2.image.color = baseColor;
        ans3.image.color = baseColor;

        questionTotal++;

        if (questionTotal < 7)
            RandomizeQuestion();
    }
    IEnumerator BadAnswer(int playerAns)
    {
        

        wrong.Play();
        ans1.interactable = false;
        ans2.interactable = false;
        ans3.interactable = false;
        fillbarQuestion.fillAmount = 0f;
        blames[currentBlame].SetActive(true);
        if (currentBlame >= 2)
        {
            Question.text = "C'est donc un 3ème blâme et un aller simple pour le bureau du CPE!";
            yield return new WaitForSeconds(1f);
            levelManager.GameOver();
            QuestionUI.SetActive(false);
            if (levelManager.isMobile == true) slider.SetActive(false);
            else UiPc.SetActive(false);
            menuTableButton.SetActive(false);

        }
        else
        {
            Question.text = "Voilà qui mérite un blâme.";
            currentBlame++;
            yield return new WaitForSecondsRealtime(1f);
            QuestionUI.SetActive(false);
            if (levelManager.isTuto == false)
            {
                if (levelManager.isMobile == true) slider.SetActive(true);
                else UiPc.SetActive(true);
                menuTableButton.SetActive(true);
            }
            levelManager.PuniCoin();
            yield return new WaitForSecondsRealtime(6f);

            cameraJeu.cullingMask = normalCMask;

            senseiAnim.SetBool("Question", false);
            ans1.image.color = baseColor;
            ans2.image.color = baseColor;
            ans3.image.color = baseColor;

            questionTotal++;
            if(questionTotal<7)
                RandomizeQuestion();


        }
       
    }

    #region
    void SetQuestionArray()
    {
        questions[0, 0] = "Type1";
        questions[1, 0] = "Veni Vidi Vici";
        questions[2, 0] = "Errare Humanum Est";
        questions[3, 0] = "Moritori Te Salutant";
        questions[4, 0] = "Quelle citation latrine";
        questions[0, 1] = "Type1";
        questions[1, 1] = "Voyage Au Centre de La Terre";
        questions[2, 1] = "20 000 Lieues sous les Mers";
        questions[3, 1] = "Le Tour du Monde en 80 jours";
        questions[4, 1] = "Quelle titre d'une Oeuvre de Jules Vernes";
        questions[0, 2] = "Type1";
        questions[1, 2] = "E=mc²";
        questions[2, 2] = "pV=nRT";
        questions[3, 2] = "U=RI";
        questions[4, 2] = "Quelle équation célèbre en physique";
        questions[0, 3] = "Type1";
        questions[1, 3] = "La Tour de Pise";
        questions[2, 3] = "La Statue de la Liberté";
        questions[3, 3] = "L'Arc de Triomphe";
        questions[4, 3] = "Le nom de quel monument historique";
        questions[0, 4] = "Type1";
        questions[1, 4] = "Baruch Spinoza";
        questions[2, 4] = "Jhon Locke";
        questions[3, 4] = "Isaac Newton";
        questions[4, 4] = "Le nom de quel philosophe à l'origine du mouvement des Lumières";
        questions[0, 5] = "Type1";
        questions[1, 5] = "Valéry Giscard d'Estaing";
        questions[2, 5] = "Georges Pompidou";
        questions[3, 5] = "François Mitterrand";
        questions[4, 5] = "Quel nom de président français de la 5ème République";
        questions[0, 6] = "Type1";
        questions[1, 6] = "Maastricht";
        questions[2, 6] = "Lisbonne";
        questions[3, 6] = "Bruxelles";
        questions[4, 6] = "Quel nom de ville ayant abrité la signature d'un traité";
        questions[0, 7] = "Type2";
        questions[1, 7] = "de Tour Eiffel";
        questions[2, 7] = "de Big Ben";
        questions[3, 7] = "de Statue de La Liberté";
        questions[4, 7] = "Monument";
        questions[0, 8] = "Type2";
        questions[1, 8] = "de pommes";
        questions[2, 8] = "de poires";
        questions[3, 8] = "de bananes";
        questions[4, 8] = "fruits";
        questions[0, 9] = "Type2";
        questions[1, 9] = "de carrés";
        questions[2, 9] = "de triangles";
        questions[3, 9] = "de ronds";
        questions[4, 9] = "figures géométriques";
        questions[0, 10] = "Type2";
        questions[1, 10] = "de signes plus";
        questions[2, 10] = "de signes égal";
        questions[3, 10] = "de signes division";
        questions[4, 10] = "opérateurs";
        questions[0, 11] = "Type2";
        questions[1, 11] = "de carottes";
        questions[2, 11] = "de patates";
        questions[3, 11] = "d'aubergines";
        questions[4, 11] = "légumes";
        questions[0, 12] = "Type2";
        questions[1, 12] = "symboles Chrétiens";
        questions[2, 12] = "symboles Juifs";
        questions[3, 12] = "symboles de l'Islam";
        questions[4, 12] = "symboles religieux";
        //etc...on va peut être essayer de récup un tableau excel text...
    }
    void SetUpDict()
    {
        dict.Add("de Tour Eiffel", images[0]);
        dict.Add("de Big Ben", images[1]);
        dict.Add("de Statue de La Liberté", images[2]);
        dict.Add("de symboles Chrétiens", images[3]);
        dict.Add("de symboles Juifs", images[4]);
        dict.Add("de symboles de l'Islam", images[5]);
        dict.Add("de pommes", images[6]);
        dict.Add("de poires", images[7]);
        dict.Add("de bananes", images[8]);
        dict.Add("de carrés", images[9]);
        dict.Add("de triangles", images[10]);
        dict.Add("de ronds", images[11]);
        dict.Add("de signes plus", images[12]);
        dict.Add("de signes égal", images[13]);
        dict.Add("de signes division", images[14]);
        dict.Add("de carottes", images[15]);
        dict.Add("de patates", images[16]);
        dict.Add("d'aubergines", images[17]);
    }
    #endregion
}
