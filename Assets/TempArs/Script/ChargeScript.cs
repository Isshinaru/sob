﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ChargeScript : MonoBehaviour
{
    [SerializeField] Slider slider;
    [SerializeField] GameObject  projPos, cursor, projPref,fakeProjoPref, projStartPos;
    [SerializeField] float powerCoeff;
    [SerializeField] LineRenderer lr;
    GameObject proj,fakeProj;
    List<GameObject> ammoPool;
    Rigidbody rb;
    Vector3 VitIn;
    float val;
    bool aim, aimStored, isMun;
    int ammoIndex;
    Color startCol, endCol;

    [SerializeField] ScriptMuniTempo muniTempo;                  //Pour les tests
    [SerializeField] LevelManager levelManager;
    
    AudioSource shoot, noAmmo;
    public float mousePower =0;
    

    //Faudait faire quand tu uses la molette tu charge le tir merci
    //Jai fait au final mais j'ai remarqué que debase y'a plein de truc qui allait pas partir d moment ou tu vises pas tout droit
    void Start()
    {
        AudioSource[] weaponAudios = GetComponents<AudioSource>();
        shoot = weaponAudios[0];
        noAmmo = weaponAudios[1];
        startCol = Color.white;
        endCol = Color.yellow;

        aim = false;
        aimStored = true;
        lr.positionCount = 270;
        fakeProj = Instantiate(fakeProjoPref, new Vector3(0, -50, 0), Quaternion.identity);
        ammoPool = new List<GameObject>();
       
        if(levelManager.isTuto)
        {
            StartCoroutine(ReloadProj());
        }
       for(int i = 0; i < 10; i++)
        {
            GameObject newProj= Instantiate(projPref, new Vector3(0, -50, 0), Quaternion.identity);
            ammoPool.Add(newProj);
        }
       
        
       

        proj = ammoPool[ammoPool.Count-1];
        ammoPool.Remove(proj);
        
       /* proj.transform.position = projPos.transform.position;
        proj.transform.parent = projPos.transform;      
        rb = proj.GetComponent<Rigidbody>();*/
    }
    void Update()
    {
     
       // proj.transform.position = projPos.transform.position;
        if (muniTempo.munition <= 0 )
        {
            slider.interactable=false;
        }
        else if(!slider.IsInteractable())
        {
            slider.interactable = true;
        }
        if (levelManager.isMobile == false)
        {
            if (Input.mouseScrollDelta.y > 0 || Input.mouseScrollDelta.y < 0)
            {
                if (muniTempo.munition == 0) return;
                aim = true;
                aimStored = false;
                if (mousePower >= 0 && mousePower <= 1)
                {
                    mousePower += Input.mouseScrollDelta.y / 100;
                    // aim = true;

                }
                if (mousePower < 0)
                    mousePower = 0;
                else if (mousePower > 1)
                    mousePower = 1;
            }
        }
        
      
        if (aim)
        {
            if (!lr.gameObject.activeInHierarchy) lr.gameObject.SetActive(true);
            if (levelManager.isMobile)
            {
                startCol.b = 1 - slider.value;
                endCol.g = 1 - slider.value;
            }
            else
            {
                startCol.b = 1 - mousePower;
                endCol.g = 1 - mousePower;
            }
               
            lr.startColor = startCol;
            lr.endColor = endCol;
            fakeProj.GetComponent<Rigidbody>().isKinematic = true;
            fakeProj.GetComponent<Rigidbody>().velocity = Vector3.zero;
            fakeProj.transform.position = projPos.transform.position;
            fakeProj.transform.parent = projPos.transform;                //Verif
            fakeProj.GetComponent<Collider>().enabled = false;
            fakeProj.GetComponent<Rigidbody>().isKinematic = false;

            if(levelManager.isMobile) fakeProj.GetComponent<Rigidbody>().AddForce(slider.value * powerCoeff * (cursor.transform.position - proj.transform.position));
            else fakeProj.GetComponent<Rigidbody>().AddForce(mousePower * powerCoeff * (cursor.transform.position - proj.transform.position));//Pc          

            StartCoroutine(VInit(fakeProj.GetComponent<Rigidbody>()));
        }
        else if (!aim&&!aimStored)
        {
            aimStored = true;
            lr.gameObject.SetActive(false);
        }
      
        if(Input.GetKeyDown(KeyCode.Mouse1))
        {
            ReleaseSlider();
        }
        

    }
    public void ReleaseSlider()
    {
        //Tir
        if (muniTempo.munition == 0)
        {
            noAmmo.Play();
            return;
        } 
        proj.transform.parent = null;
        aim = false;
        rb.isKinematic = false;
        shoot.Play();
        if (levelManager.isMobile) rb.AddForce(slider.value * powerCoeff * (cursor.transform.position - proj.transform.position)); //Tel
        else rb.AddForce(mousePower * powerCoeff * (cursor.transform.position - proj.transform.position));//Pc            

        slider.value = slider.minValue;
        mousePower = 0;
        if (GameObject.Find("TutoManager"))     //C'est pas ouf mais je vais chercher mieux
            muniTempo.MunForTuto();
        else
            muniTempo.MunMoins();
        if(muniTempo.munition>=1)
            StartCoroutine(ReloadProj());

    }
    public void PreviewTrajectory()
    {
        if (muniTempo.munition == 0) return;
        aim = true;
        aimStored = false;
    }
    void TrajEq()
    {
        Vector3 vInit = VitIn;
        float time=0;
        Vector3[] arcArray = new Vector3[270];
        for(int i = 0; i <lr.positionCount; i++)
        {
            time += 0.01f;
            arcArray[i]= proj.transform.position + time * vInit - 4.9f * time * time * Vector3.up;
        }
        lr.SetPositions(arcArray);
    }
    IEnumerator VInit(Rigidbody rig)
    {
        yield return new WaitForFixedUpdate();
        VitIn = rig.velocity;
        TrajEq();
    }
    public void StoreProjInPool(GameObject proj)    //Bon ce truc me fait parfois des trucs bizzares (appelé par le scriptboulette) donc j'ai retiré en attendant
    {
       /* proj.GetComponent<Rigidbody>().isKinematic = true;
        proj.GetComponent<Rigidbody>().velocity = Vector3.zero;
        proj.transform.position = new Vector3(0f, -50f, 0f);*/
        ammoPool.Add(proj);
    }
    public IEnumerator ReloadProj()
    {
        
        yield return new WaitForSecondsRealtime(0.2f);
        if (muniTempo.munition > 0)
        {
            if (ammoPool.Count == 0)
            {
                GameObject newProj = Instantiate(projPref, new Vector3(0, -50, 0), Quaternion.identity);

                ammoPool.Add(newProj);
            }
            proj = ammoPool[ammoPool.Count - 1];
            ammoPool.Remove(proj);
            proj.transform.position = projPos.transform.position;
            proj.transform.parent = projPos.transform;                      //Verif
            rb = proj.GetComponent<Rigidbody>();
        }
       
    }
}
