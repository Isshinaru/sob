﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] Camera mainCam, tableCam;
    [SerializeField] Canvas mainCanvas, tableCanvas;
    [SerializeField] GameObject questionUi, scrollImage, rightcliclImage, slider;
    bool gyroEnabled;
    Gyroscope gyros;
    GameObject cameraContainer;
    Quaternion rot;
    public float sensitivity = 10;
    float rotationX, rotationY;
    [SerializeField] LevelManager levelManager;
    void Start()
    {
        cameraContainer = new GameObject("Camera Container");
        cameraContainer.transform.position = transform.position;
        transform.SetParent(cameraContainer.transform);
        gyroEnabled = EnableGyro();
    }

    bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            levelManager.isMobile = true;
            slider.SetActive(true);
            scrollImage.SetActive(false);
            rightcliclImage.SetActive(false);
            gyros = Input.gyro;
            gyros.enabled = true;
            cameraContainer.transform.rotation = Quaternion.Euler(90f, 0f, 0f);
            rot = new Quaternion(0, 0, 1, 0);
            return true;
        }
        else
        {
            levelManager.isMobile = false;
            scrollImage.SetActive(true);
            rightcliclImage.SetActive(true);
            slider.SetActive(false);
            return false;
        }
        
    }

    private void Update()
    {
        if (questionUi.activeSelf)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);    //Temporaire
        }
        else
        {
            if (gyroEnabled)
            {
                transform.localRotation = gyros.attitude * rot;
            }
            else
            {
                rotationX -= Input.GetAxis("Mouse Y") * sensitivity;                   //Pour la cam pc
                if (rotationX > 70f) rotationX = 70;

                else if (rotationX < -70) rotationX = -70;
                rotationY += Input.GetAxis("Mouse X") * sensitivity;
                transform.localRotation = Quaternion.Euler(rotationX, rotationY, 0);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    if (mainCam.enabled == false) SwitchCam(1);
                    else SwitchCam(0);
                }
            }
        }
       
        
           

    }
    public void SwitchCam(int i)
    {
        switch(i)
            {
            case 0:
                mainCanvas.enabled = false;
                tableCam.enabled = true;
                mainCam.enabled = false;
                tableCanvas.enabled = true;
                break;
            case 1:
                tableCanvas.enabled = false;
                mainCam.enabled = true;
                tableCam.enabled = false;
                mainCanvas.enabled = true;
                break;
            default:
                break;
        }
    }
}
