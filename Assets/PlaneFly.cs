﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlaneFly : MonoBehaviour
{
    [SerializeField] ScoreManager scoreManager;
    [SerializeField] int rank, posInRoom;
    [SerializeField] List<GameObject> planePos;
    [SerializeField] TextMeshPro scoreText;
    AudioSource crashSound;
    ParticleSystem boum;
    Rigidbody rb;
    Vector3 startPos;
    Collider coll;
    int rankTotal;
    bool interact = false;
    // Start is called before the first frame update
    void Start()
    {
        crashSound = GetComponent<AudioSource>();
        scoreText.text = "";
        startPos = transform.position;
        rb = gameObject.GetComponent<Rigidbody>();
        coll = gameObject.GetComponent<BoxCollider>();
        boum = gameObject.GetComponent<ParticleSystem>();
        StartCoroutine(Moving());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void TpPosition()
    {
        StopAllCoroutines();
        StartCoroutine(Kaboum());
    }
    void OnCollisionEnter(Collision other)
    {
     
        if (other.gameObject.layer == 10 && interact)
        {
            scoreManager.Score(1, 5, rankTotal, 3, 1);
            scoreText.text = "+ " + 5 * rankTotal * 3 * 1;
            scoreText.transform.LookAt(2 * scoreText.transform.position - Camera.main.transform.position);
            crashSound.Play();
            StopAllCoroutines();
            StartCoroutine(Kaboum());
        }
       /* if (other.gameObject.tag == "Wall" && interact)
            TpPosition();*/

    }
    IEnumerator Moving()
    {
        interact = true;
        yield return new WaitForSeconds(2f);
        int a = Random.Range(0, planePos.Count);
        switch(a)
        {
            case 0:
                rankTotal = 3 + rank;
                break;
            case 1:
                rankTotal = 6 + rank;
                break;
            case 2:
                rankTotal = 9 + rank;
                break;
            case 3:
                rankTotal = 12 + rank;
                break;
            case 4:
                rankTotal = 15 + rank;
                break;
            default:
                break;
        }
        int speed = Random.Range(5, 10);
        if(posInRoom==0)
        {
            while (transform.position.x < planePos[a].transform.position.x)
            {
                
                transform.position = Vector3.MoveTowards(transform.position, planePos[a].transform.position + new Vector3(1,0,0), Time.deltaTime * speed);
              
               
                yield return new WaitForEndOfFrame();
            }
            
        }
        else
        {
            while (transform.position.x > planePos[a].transform.position.x)
            {
                transform.position = Vector3.MoveTowards(transform.position, planePos[a].transform.position - new Vector3(1, 0, 0), Time.deltaTime * speed);
                yield return new WaitForEndOfFrame();
            }
        }
        TpPosition();
      
    }
    IEnumerator Kaboum()
    {
        
        interact = false;
        coll.enabled = false;
        rb.useGravity = true;
        boum.Play();
        yield return new WaitForSeconds(1f);
        coll.enabled = true;
        rb.useGravity = false;
        yield return new WaitForSeconds(1f);
        transform.position = startPos;
        boum.Stop();
        scoreText.text = "";
        yield return new WaitForSeconds(2f);
        StartCoroutine(Moving());
    }
}
