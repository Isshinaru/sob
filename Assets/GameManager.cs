﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    int maxScore;
    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    public void NewScore()
    {
        maxScore = GameObject.Find("ScoreManager").GetComponent<ScoreManager>().player1Score;
    }
    
}
